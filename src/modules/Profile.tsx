import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Alert
} from 'react-native';
import { Colors} from 'react-native/Libraries/NewAppScreen';

import auth from '@react-native-firebase/auth';
import functions from '@react-native-firebase/functions';
import firestore from "@react-native-firebase/firestore"

import AsyncStorage from '@react-native-async-storage/async-storage';

const Profile = ({userID}: any) => {  
  const [name, setName] = React.useState('')
  const [age, setAge] = React.useState('')
  const [number, setNumber] = React.useState('')
  const [color, setColor] = React.useState('')
  
  // Realtime changes of user data
  React.useEffect(() => {
    const subscriber = firestore()
  .collection('users')
  .doc(userID)
  .onSnapshot((documentSnapshot: any) => {
    const data = documentSnapshot.data();
    setName(data?.name);
    setAge(data?.age);
    setNumber(data?.number);
    setColor(data?.color);
  });
  return () => subscriber();
}, [userID]);

  const updateProfile = async () => {
    const firebaseFunction =  functions().httpsCallable('updateProfile');
    ////////////////////////////////////////////////
    //await AsyncStorage.getItem('@firebase_uid')
    ////////////////////////////////////////////////
    const firebaseData = { 
      userID: userID,
      name: name,
      age: age,
      number: number,
      color: color 
    };

    await firebaseFunction(firebaseData)
      .then((result : any) => {
        console.log('result', result)
        result.data ? Alert.alert(
          "Success",
          "You Profile has been updated successfully",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        )
        :
        Alert.alert(
          "Failed",
          "You missed some of fields. All fields are required",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        )
      })
      .catch((error: any) => {
        Alert.alert(
          "Failed",
          error.message,
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      });
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Here is Profile page with user data</Text>
              <View style = {styles.row}>
                <Text style={styles.sectionDescription}>name</Text>
                <TextInput 
                  placeholder = {'enter name'}
                  value = {name}
                  onChangeText = {setName}
                />
              </View>

              <View style = {styles.row}>
                <Text style={styles.sectionDescription}>age</Text>
                <TextInput 
                  placeholder = {'enter age'}
                  value = {age}
                  onChangeText = {setAge}
                />
              </View>

              <View style = {styles.row}>
                <Text style={styles.sectionDescription}>number</Text>
                <TextInput 
                  placeholder = {'enter number'}
                  value = {number}
                  onChangeText = {setNumber}
                />
              </View>

              <View style = {styles.row}>
                <Text style={styles.sectionDescription}>color</Text>
                <TextInput 
                  placeholder = {'enter color'}
                  value = {color}
                  onChangeText = {setColor}
                />
              </View>

            </View>
          </View>

          <View style = {styles.button}>
            <Button 
              title = 'Update profile'
              onPress = {updateProfile}
            />
          </View>

          <View style = {styles.button}>
            <Button 
              title = 'Log out'
              onPress = {() => auth().signOut().then(() => console.log('User signed out!'))}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.white,
  },
  row: {
    display: 'flex',
    justifyContent: 'center'
  },
  button: {
    margin: 20,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  }
});

export default Profile;
